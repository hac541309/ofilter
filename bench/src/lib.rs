#![feature(test)]
extern crate bloomfilter;
extern crate ofilter;
extern crate test;

#[cfg(test)]
mod tests {
    use super::*;
    use bloom;
    use bloomfilter;
    use ofilter::*;
    use std::collections::HashSet;
    use test::Bencher;

    const CAPACITY: usize = 100_000;

    #[bench]
    fn bench_extern_crate_bloomfilter(b: &mut Bencher) {
        let params = Params::with_nb_items_and_fp_rate(CAPACITY, 0.01);
        let mut filter: bloomfilter::Bloom<isize> =
            bloomfilter::Bloom::new_for_fp_rate(params.nb_items, params.fp_rate);
        // A note about this bench -> the bloomfilter::Bloom and the
        // local Bloom typically do not have the same bit_len. This is
        // because as I am writing these lines, the bit_len calculated
        // by the bloomfilter::Bloom is based on the *OPTIMAL* number
        // of hash, which is 7 for this example. But it caps the number
        // of hash to 2, so you'd need to add bits to compensate.
        // Local Bloom does this, but it's not slower anyway.
        //
        // TL;DR ofilter::Bloom uses more bits but is faster anyway.
        let mut dummy = false;
        let mut i: isize = 0;
        b.iter(|| {
            filter.set(&i);
            dummy = filter.check(&i);
            i += 1;
        });
        println!("dummy: {}", dummy);
    }

    #[bench]
    fn bench_extern_crate_bloom(b: &mut Bencher) {
        let params = Params::with_nb_items_and_fp_rate(CAPACITY, 0.01);
        let mut filter =
            bloom::BloomFilter::with_rate(params.fp_rate as f32, params.nb_items as u32);
        let mut dummy = false;
        let mut i: isize = 0;
        b.iter(|| {
            filter.insert(&i);
            dummy = filter.contains(&i);
            i += 1;
        });
        println!("dummy: {}", dummy);
    }

    #[bench]
    fn bench_standard_hashset(b: &mut Bencher) {
        let params = Params::with_nb_items_and_fp_rate(CAPACITY, 0.01);
        let mut filter: HashSet<isize> = HashSet::with_capacity(params.nb_items);
        let mut dummy = false;
        let mut i: isize = 0;
        b.iter(|| {
            filter.insert(i);
            dummy = filter.contains(&i);
            i += 1;
        });
        println!("dummy: {}", dummy);
    }

    #[bench]
    fn bench_ofilter_bloom(b: &mut Bencher) {
        let mut filter: Bloom<isize> = Bloom::new(CAPACITY);
        let mut i: isize = 0;
        let mut dummy = false;
        b.iter(|| {
            filter.set(&i);
            dummy = filter.check(&i);
            i += 1;
        });
        println!("dummy: {}", dummy);
    }

    #[bench]
    fn bench_ofilter_sync_bloom(b: &mut Bencher) {
        let filter: SyncBloom<isize> = SyncBloom::new(CAPACITY);
        let mut i: isize = 0;
        let mut dummy = false;
        b.iter(|| {
            filter.set(&i);
            dummy = filter.check(&i);
            i += 1;
        });
        println!("dummy: {}", dummy);
    }

    #[bench]
    fn bench_ofilter_stream(b: &mut Bencher) {
        let mut filter: Stream<isize> = Stream::new(CAPACITY);
        let mut i: isize = 0;
        let mut dummy = false;
        b.iter(|| {
            filter.set(&i);
            dummy = filter.check(&i);
            i += 1;
        });
        println!("dummy: {}", dummy);
    }

    #[bench]
    fn bench_ofilter_sync_stream(b: &mut Bencher) {
        let filter: SyncStream<isize> = SyncStream::new(CAPACITY);
        let mut i: isize = 0;
        let mut dummy = false;
        b.iter(|| {
            filter.set(&i);
            dummy = filter.check(&i);
            i += 1;
        });
        println!("dummy: {}", dummy);
    }
}
